import 'package:cocktail_adventure/blocs/cocktail_cubit.dart';
import 'package:cocktail_adventure/repository/cocktail_repository.dart';
import 'package:cocktail_adventure/repository/preferences_repository.dart';
import 'package:cocktail_adventure/repository/repository.dart';
import 'package:cocktail_adventure/ui/screens/cocktail_detail.dart';
import 'package:cocktail_adventure/ui/screens/home.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final PreferencesRepository preferencesRepository = PreferencesRepository();
  final CocktailRepository cocktailRepository = CocktailRepository();
  final Repository repository = Repository(cocktailRepository, preferencesRepository);
  final CocktailCubit cocktailCubit = CocktailCubit(repository);

  await repository.loadRandom();
  await cocktailCubit.loadFavorites();

  runApp(
      MultiProvider(
        providers: [
          Provider<CocktailCubit>(create: (_) => cocktailCubit),
          Provider<Repository>(create: (_) => repository),
        ],
        child: const MyApp(),
      )
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(debugShowCheckedModeBanner: false,
      title: 'Cocktail Adventure',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: {
        '/home': (context) => const Home(),
        '/fav-detail': (context) => const CocktailDetail(),
      },
      initialRoute: "/home",
    );
  }
}

