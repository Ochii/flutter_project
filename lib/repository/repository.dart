import 'package:cocktail_adventure/models/cocktail.dart';
import 'package:cocktail_adventure/repository/cocktail_repository.dart';
import 'package:cocktail_adventure/repository/preferences_repository.dart';

class Repository {
  final CocktailRepository _cocktailRepository;
  final PreferencesRepository _preferencesRepository;

  Future<List<Cocktail>> searchCocktailByName(String query) => _cocktailRepository.fetchCocktails('/search.php', 's', query);

  Future<Cocktail> loadRandom() => _cocktailRepository.fetchRandom();

  Future<void> saveFavorite(List<Cocktail> cocktail) async => _preferencesRepository.saveCocktail(cocktail);

  Future<List<Cocktail>> loadFavorites() async =>_preferencesRepository.loadFavorite();

  Repository(this._cocktailRepository, this._preferencesRepository);
}