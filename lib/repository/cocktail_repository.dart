import 'dart:convert';

import 'package:cocktail_adventure/models/cocktail.dart';
import 'package:http/http.dart';

class CocktailRepository {
  Future<List<Cocktail>> fetchCocktails(String type, String queryLetter, String queryValue) async {
    Response response = await get(
        Uri.https('www.thecocktaildb.com',
        '/api/json/v1/1$type', {
          queryLetter:queryValue
        })
    );
    if(response.statusCode == 200) {
      List<Cocktail> cocktails = [];
      Map<String, dynamic> json = jsonDecode(response.body);
      if(json.containsKey("drinks")) {
        List<dynamic> features = json['drinks'];
        for (var feature in features) {
          cocktails.add(Cocktail.fromJsonApi(feature));
        }
      }
      return cocktails;
    } else {
      throw Exception('Failed to load addresses');
    }
  }

  Future<Cocktail> fetchRandom() async {
    Response response = await get(Uri.https('thecocktaildb.com', '/api/json/v1/1/random.php'));
    if(response.statusCode == 200) {
      Map<String, dynamic> json = jsonDecode(response.body);
      return Cocktail.fromJsonApi(json['drinks'][0]);
    } else {
      throw Exception('Failed to load addresses');
    }
  }
}