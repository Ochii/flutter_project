import 'dart:convert';

class Cocktail {
  int id;
  String? name;
  String? category;
  String? instructionsEn;
  String? instructionsFr;
  String? instructionsDe;
  String? instructionsIt;
  String? imgUrl;
  Map<String, String?> ingredients; // key = name; value = measure

  String toJson() {
    return jsonEncode({
      'id': id,
      'name': name,
      'category': category,
      'instructionsEn': instructionsEn,
      'instructionsFr': instructionsFr,
      'instructionsDe': instructionsDe,
      'instructionsIt': instructionsIt,
      'imgUrl': imgUrl,
      'ingredients': ingredients,
    });
  }

  /// Hydrate data from API
  factory Cocktail.fromJsonApi(Map<String, dynamic> map) {
    int _id = int.parse(map['idDrink']);
    String? _name = map['strDrink'];
    String? _category = map['strCategory'];
    String? _instructionsEn = map['strInstructions'];
    String? _instructionsFr = map['strInstructionsFR'];
    String? _instructionsDe = map['strInstructionsDE'];
    String? _instructionsIt = map['strInstructionsIT'];
    String? _imgUrl = map['strDrinkThumb'];

    // Iterate the json to catch strIngredient1, strIngredient2, ...
    Map<String, String?> _ingredients = {};
    map.forEach((key, value) {
      if(key.isNotEmpty && key.startsWith('strIngredient') && value!=null) {
        String index = key.substring(13);
        String? _measure = map['strMeasure$index'];
        _ingredients[value] = _measure;
      }
    });
    return Cocktail(_id, _name, _category, _instructionsEn, _instructionsFr,
        _instructionsDe, _instructionsIt, _imgUrl, _ingredients);
  }

  factory Cocktail.fromJsonPreferences(String json) {
    Map<String, dynamic> map = jsonDecode(json);
    Map<String, String?> _ingredients = {};
    map['ingredients'].forEach((key, value) {
      if(key != null && value != null) {
        _ingredients[key] = value;
      }
    });

    return Cocktail(map['id']
        , map['name']
        , map['category']
        , map['instructionsEn']
        , map['instructionsFr']
        , map['instructionsDe']
        , map['instructionsIt']
        , map['imgUrl']
        , _ingredients);
  }

  Cocktail(this.id, this.name, this.category, this.instructionsEn, this.instructionsFr,
      this.instructionsDe, this.instructionsIt, this.imgUrl, this.ingredients);
}