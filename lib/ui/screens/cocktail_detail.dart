import 'package:cocktail_adventure/models/cocktail.dart';
import 'package:cocktail_adventure/ui/widgets/cocktail_view.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CocktailDetail extends StatelessWidget {
  const CocktailDetail({Key? key}) : super(key: key);
  static const routeName = "/fav-detail";

  @override
  Widget build(BuildContext context) {
    final Map<String, dynamic> args = ModalRoute.of(context)?.settings.arguments as Map<String, Cocktail>;
    final Cocktail cocktail = args['cocktail'];
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
            "Return to favorites",
            style: GoogleFonts.dancingScript(
            fontSize: 32,
            fontWeight: FontWeight.bold,
            color: Colors.black
            )
        ),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          color: Colors.black,
          onPressed: () {
            Navigator.pop(context);
          },
          iconSize: 20,
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
            child: CocktailView(cocktail)
          )
        ),
    );
  }

}