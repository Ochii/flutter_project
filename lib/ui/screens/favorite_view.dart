import 'package:cached_network_image/cached_network_image.dart';
import 'package:cocktail_adventure/blocs/cocktail_cubit.dart';
import 'package:cocktail_adventure/models/cocktail.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import 'cocktail_detail.dart';

class FavoriteView extends StatefulWidget {
  const FavoriteView({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _FavoriteView();
}

class _FavoriteView extends State<FavoriteView> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CocktailCubit, List<Cocktail>>(
      builder: (context, _cocktails){
          return SingleChildScrollView(
              child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'My favorites',
                        style: GoogleFonts.dancingScript(
                            fontSize: 32,
                            fontWeight: FontWeight.bold
                        ),
                      ),
                      ListView.builder(
                        padding: const EdgeInsets.all(8),
                        itemCount: _cocktails.length,
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemBuilder: (BuildContext context, int index) {
                          return Dismissible(
                            key: UniqueKey(),
                            onDismissed: (direction) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  duration: const Duration(milliseconds: 500),
                                    content: Text('${_cocktails[index].name} has been remove')),
                              );
                              setState(() {
                                _cocktails.removeAt(index);
                                Provider.of<CocktailCubit>(context, listen: false).saveFavorites(_cocktails);

                              });
                            },
                            child: SizedBox(
                              height: 90,
                              child: Card(
                                elevation: 9,
                                shape: const RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(15)),
                              ),
                              child: InkWell(
                                onTap: () {
                                  Navigator.pushNamed(context, CocktailDetail.routeName,
                                    arguments: { "cocktail":_cocktails[index]});
                                },
                                child: ListTile(
                                      leading: Hero(
                                        tag: "detail${_cocktails[index].id}",
                                        child: CachedNetworkImage(
                                          imageUrl: _cocktails[index].imgUrl ?? '',
                                          width: 70,
                                          height: 70,
                                          placeholder: (context, url) => const CircularProgressIndicator(),
                                          errorWidget: (context, url, error) => const Icon(Icons.error),
                                        ),
                                      ),
                                      title: Text(_cocktails[index].name ?? ''),
                                      subtitle:  Text(_cocktails[index].category?? ''),
                                      trailing: SizedBox(
                                        width: 36.0,
                                        child: Row(
                                          children: const [
                                            Icon(Icons.delete, size: 18),
                                            Icon(Icons.arrow_forward, size: 18),
                                          ],
                                        ),
                                      )
                                    ),
                              ),
                                ),
                            ),
                          );
                        },
                      ),
                    ]
              ),
          );
      }
    );
  }
}