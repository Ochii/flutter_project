import 'package:cocktail_adventure/models/cocktail.dart';
import 'package:cocktail_adventure/repository/repository.dart';
import 'package:cocktail_adventure/ui/widgets/cocktail_view.dart';
import 'package:cocktail_adventure/ui/screens/favorite_view.dart';
import 'package:cocktail_adventure/ui/screens/search_view.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _Home();
}

class _Home extends State<Home> {
  int _selectedIndex = 1;

  final Widget _homePage = _HomePage();
  final Widget _favoriteView = const FavoriteView();
  final Widget _searchView = const SearchView();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: getBody(),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items:  const [
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: "Search",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: "Home",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: "Favorites",
          )
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: (int index) => {
          setState(() {
            _selectedIndex = index;
          })
        },
      ),
    );
  }

  Widget getBody() {
    if(_selectedIndex == 0) {
      return _searchView;
    } else if (_selectedIndex == 1) {
      return _homePage;
    } else {
      return _favoriteView;
    }
  }
}

class _HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'A random cocktail',
            style: GoogleFonts.dancingScript(
            fontSize: 32,
            fontWeight: FontWeight.bold
            ),
          ),
          const SizedBox(height: 15),
          FutureBuilder<Cocktail>(
            future: Provider.of<Repository>(context, listen: false).loadRandom(),
            builder: (context, snapshot) {
              if(!snapshot.hasData) {
                return const Text("Loading...");
              }
              return CocktailView(snapshot.data!);
            }
          )
        ],
      ),
    );
  }
}
