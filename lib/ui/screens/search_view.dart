import 'package:cocktail_adventure/blocs/cocktail_cubit.dart';
import 'package:cocktail_adventure/models/cocktail.dart';
import 'package:favorite_button/favorite_button.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import 'cocktail_detail.dart';

class SearchView extends StatefulWidget{
  const SearchView({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SearchView();

}

class _SearchView extends State<SearchView> {
  final _textFieldController = TextEditingController();

  List<Cocktail> _cocktails = [];
  @override
  Widget build(BuildContext context) {
    List<Cocktail> favorites = Provider.of<CocktailCubit>(context, listen:false).state;
    return Column(
      children: [
        Text(
          'Search a cocktail',
          style: GoogleFonts.dancingScript(
              fontSize: 32,
              fontWeight: FontWeight.bold
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
          child: TextFormField(
            controller: _textFieldController,
            onChanged: (String value) async {
              if (value.isNotEmpty) {
                List<Cocktail> result = await Provider.of<CocktailCubit>(
                    context, listen: false).searchCocktailByName(value);
                setState(() {
                  _cocktails = result;
                });
              }
            },
            decoration: InputDecoration(
              prefixIcon: const Icon(Icons.search),
              hintText: 'Enter cocktail name',
              labelText: 'Cocktail name *',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
        ),
        Expanded(
          child: SingleChildScrollView(
            child: ListView.builder(
              padding: const EdgeInsets.all(8),
              itemCount: _cocktails.length,
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemBuilder: (BuildContext context, int index) {
                return Dismissible(
                  key: UniqueKey(),
                  onDismissed: (direction) {
                    setState(() {
                      _cocktails.removeAt(index);
                    });
                  },
                  child: SizedBox(
                    height: 90,
                    child: Card(
                      elevation: 9,
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(15)),
                      ),
                      child: InkWell(
                        onTap: () {
                          Navigator.pushNamed(context, CocktailDetail.routeName,
                              arguments: { "cocktail":_cocktails[index]});
                        },
                        child: ListTile(
                            leading: Hero(
                              tag: "detail${_cocktails[index].id}",
                              child: Image(
                                image: NetworkImage(_cocktails[index].imgUrl ?? ''),
                                width: 70,
                                height: 70,
                              ),
                            ),
                            title: Text(_cocktails[index].name ?? ''),
                            subtitle: Text(_cocktails[index].category ?? ''),
                            trailing: FavoriteButton(
                              isFavorite:  favorites.any((element) => element.id ==_cocktails[index].id),
                              valueChanged: (_isFavorite) {
                                if(_isFavorite) {
                                  Provider.of<CocktailCubit>(context, listen: false).addFavorite(_cocktails[index]);
                                } else {
                                  favorites.removeAt(favorites.indexWhere((element) => element.id == _cocktails[index].id));
                                  Provider.of<CocktailCubit>(context, listen: false).saveFavorites(favorites);
                                }
                              }
                            )
                        ),
                      )
                    ),
                  ),
                );
              },
            ),
          ),
        )
      ],
    );
  }

}