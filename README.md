# cocktail_adventure

Mathieu Mesnil project for school

# API

This project use APIs of [TheCocktailDb](https://www.thecocktaildb.com/api.php) website

## Home page

Display :
 - a random cocktail. [API used](https://www.thecocktaildb.com/api/json/v1/1/random.php)

Actions :
 - add/remove the cocktail to your favorite by tap on heart icon.

# Favorites page

Display :
 - The list of your storage favorites.

Actions :
 - Clic on a tile to display detail of the cocktail.
 - Swipe right to remove the cocktail to your favorites.

# Search page

Display :
 - An input field to search specifics cocktails
 - a list of cocktails dynamically refreshed by the input field. [API used](https://www.thecocktaildb.com/api/json/v1/1/search.php?s=margarita)

Actions :
 - Clic on a tile to display detail of the cocktail.
 - add/remove the cocktail to your favorite by tap on heart icon.
